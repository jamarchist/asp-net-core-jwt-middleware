﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Middleware.Login
{
    public static class LoginEndpointExtensions
    {
        public static IApplicationBuilder UseLoginEndpoint(this IApplicationBuilder builder, Action<LoginEndpointOptions> options = null)
        {
            var configuration = new LoginEndpointOptions();
            var configureOptions = options ?? (o => { });
            configureOptions(configuration);

            return builder.Map(configuration.Path, app => { app.UseMiddleware<LoginEndpoint>(); });
        }

        public static IServiceCollection AddLoginEndpoint<TLoginHandler>(this IServiceCollection services) where TLoginHandler : class, ILoginHandler
        {
            services.AddTransient<ILoginRequestReader, JsonLoginRequestReader>();
            services.AddTransient<ILoginResponseWriter, JsonLoginResponseWriter>();
            services.AddTransient<ILoginHandler, TLoginHandler>();

            return services;
        }
    }
}
