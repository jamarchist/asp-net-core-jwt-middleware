﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Middleware.Login
{
    public class LoginEndpoint
    {
        private readonly RequestDelegate next;

        public LoginEndpoint(RequestDelegate next)
        {
            this.next = next;
        }

        public Task Invoke(HttpContext httpContext, ILoginHandler loginHandler, ILoginRequestReader requestReader, ILoginResponseWriter responseWriter)
        {
            try
            {
                var request = requestReader.Read(httpContext);
                var result = loginHandler.LoginWithPassword(request.UserName, request.Password);
                responseWriter.WriteResponse(httpContext, result);
            }
            catch (Exception e)
            {
                responseWriter.WriteResponseOnError(httpContext, e);
            }

            return Task.CompletedTask;
        }
    }
}