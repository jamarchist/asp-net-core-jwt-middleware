﻿namespace Middleware.Login
{
    public interface ILoginHandler
    {
        LoginResult LoginWithPassword(string user, string password);
    }
}