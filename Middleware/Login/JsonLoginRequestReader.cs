﻿using System.IO;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;

namespace Middleware.Login
{
    public class JsonLoginRequestReader : ILoginRequestReader
    {
        public LoginRequest Read(HttpContext httpContext)
        {
            var body = new StreamReader(httpContext.Request.Body).ReadToEnd();
            var jsonBody = JObject.Parse(body);
            var userName = jsonBody["userName"].Value<string>();
            var password = jsonBody["password"].Value<string>();

            return new LoginRequest
            {
                UserName = userName, Password = password
            };
        }
    }
}