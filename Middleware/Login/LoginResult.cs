﻿namespace Middleware.Login
{
    public class LoginResult
    {
        public bool LoginSucceeded { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}