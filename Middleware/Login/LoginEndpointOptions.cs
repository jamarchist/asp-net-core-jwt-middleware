﻿namespace Middleware.Login
{
    public class LoginEndpointOptions
    {
        public LoginEndpointOptions()
        {
            Path = "/login";
        }

        public string Path { get; set; }
    }
}