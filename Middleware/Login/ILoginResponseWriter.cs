﻿using System;
using Microsoft.AspNetCore.Http;

namespace Middleware.Login
{
    public interface ILoginResponseWriter
    {
        void WriteResponse(HttpContext httpContext, LoginResult result);
        void WriteResponseOnError(HttpContext httpContext, Exception error);
    }
}