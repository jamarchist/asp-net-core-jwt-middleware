﻿using System;
using System.Net;
using System.Text;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Middleware.Login
{
    public class JsonLoginResponseWriter : ILoginResponseWriter
    {
        public void WriteResponse(HttpContext httpContext, LoginResult result)
        {
            if (result.LoginSucceeded)
            {
                httpContext.Response.StatusCode = (int)HttpStatusCode.OK;
                httpContext.Response.ContentType = "application/json";
                httpContext.Response.Body.Write(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(new
                {
                    access_token = result.AccessToken,
                    refresh_token = result.RefreshToken
                })));
            }
            else
            {
                httpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            }
        }

        public void WriteResponseOnError(HttpContext httpContext, Exception error)
        {
            httpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
        }
    }
}