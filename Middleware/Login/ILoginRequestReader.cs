﻿using Microsoft.AspNetCore.Http;

namespace Middleware.Login
{
    public interface ILoginRequestReader
    {
        LoginRequest Read(HttpContext httpContext);
    }
}
