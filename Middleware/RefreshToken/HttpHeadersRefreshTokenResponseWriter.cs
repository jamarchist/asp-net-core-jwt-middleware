﻿using System;
using System.Net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features.Authentication;

namespace Middleware.RefreshToken
{
    public class HttpHeadersRefreshTokenResponseWriter : IRefreshTokenResponseWriter
    {
        private readonly IClaimsPrincipalReader tokenReader;

        public HttpHeadersRefreshTokenResponseWriter(IClaimsPrincipalReader tokenReader)
        {
            this.tokenReader = tokenReader;
        }

        public void WriteResponseForSuccessfulExchange(HttpContext httpContext, RefreshTokenResult exchangeResult)
        {
            var claimsPrincipal = tokenReader.ReadClaimsFromValidToken(exchangeResult.AccessToken);
            httpContext.Features.Set<IHttpAuthenticationFeature>(new HttpAuthenticationFeature { User = claimsPrincipal });

            httpContext.Response.StatusCode = (int)HttpStatusCode.OK;
            httpContext.Response.Headers["AccessToken"] = exchangeResult.AccessToken;
            httpContext.Response.Headers["RefreshToken"] = exchangeResult.RefreshToken;
        }

        public void WriteResponseForInvalidRefreshToken(HttpContext httpContext)
        {
            httpContext.Response.StatusCode = (int) HttpStatusCode.Unauthorized;
        }

        public void WriteResponseForError(HttpContext httpContext, Exception error)
        {
            httpContext.Response.StatusCode = (int) HttpStatusCode.BadRequest;
        }
    }
}