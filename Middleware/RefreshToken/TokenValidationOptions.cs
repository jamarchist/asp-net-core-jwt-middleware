﻿using Microsoft.IdentityModel.Tokens;

namespace Middleware.RefreshToken
{
    public class TokenValidationOptions
    {
        private TokenValidationParameters expiredValidationParameters;

        public TokenValidationParameters ValidationParameters { get; set; }
        public TokenValidationParameters ExpiredValidationParameters
        {
            get
            {
                if (expiredValidationParameters != null) return expiredValidationParameters;
                if (ValidationParameters == null) return null;

                var expiredParameters = new TokenValidationParameters
                {
                    ActorValidationParameters = ValidationParameters.ActorValidationParameters,
                    AudienceValidator = ValidationParameters.AudienceValidator,
                    // If the ValidationParameters.AuthenticationType is null, this throws an error
                    //AuthenticationType = ValidationParameters.AuthenticationType,
                    ClockSkew = ValidationParameters.ClockSkew,
                    CryptoProviderFactory = ValidationParameters.CryptoProviderFactory,
                    IssuerSigningKey = ValidationParameters.IssuerSigningKey,
                    IssuerSigningKeyResolver = ValidationParameters.IssuerSigningKeyResolver,
                    IssuerSigningKeyValidator = ValidationParameters.IssuerSigningKeyValidator,
                    IssuerSigningKeys = ValidationParameters.IssuerSigningKeys,
                    IssuerValidator = ValidationParameters.IssuerValidator,
                    LifetimeValidator = ValidationParameters.LifetimeValidator,
                    NameClaimType = ValidationParameters.NameClaimType,
                    NameClaimTypeRetriever = ValidationParameters.NameClaimTypeRetriever,
                    PropertyBag = ValidationParameters.PropertyBag,
                    RequireExpirationTime = ValidationParameters.RequireExpirationTime,
                    RequireSignedTokens = ValidationParameters.RequireSignedTokens,
                    RoleClaimType = ValidationParameters.RoleClaimType,
                    RoleClaimTypeRetriever = ValidationParameters.RoleClaimTypeRetriever,
                    SaveSigninToken = ValidationParameters.SaveSigninToken,
                    SignatureValidator = ValidationParameters.SignatureValidator,
                    TokenDecryptionKey = ValidationParameters.TokenDecryptionKey,
                    TokenDecryptionKeyResolver = ValidationParameters.TokenDecryptionKeyResolver,
                    TokenDecryptionKeys = ValidationParameters.TokenDecryptionKeys,
                    TokenReader = ValidationParameters.TokenReader,
                    TokenReplayCache = ValidationParameters.TokenReplayCache,
                    TokenReplayValidator = ValidationParameters.TokenReplayValidator,
                    ValidAudience = ValidationParameters.ValidAudience,
                    ValidAudiences = ValidationParameters.ValidAudiences,
                    ValidIssuer = ValidationParameters.ValidIssuer,
                    ValidIssuers = ValidationParameters.ValidIssuers,
                    ValidateActor = ValidationParameters.ValidateActor,
                    ValidateAudience = ValidationParameters.ValidateAudience,
                    ValidateIssuer = ValidationParameters.ValidateIssuer,
                    ValidateIssuerSigningKey = ValidationParameters.ValidateIssuerSigningKey,
                    // This is the only difference... we need to turn this off in order to read the claims from an expired key
                    ValidateLifetime = false,
                    ValidateTokenReplay = ValidationParameters.ValidateTokenReplay
                };

                if (ValidationParameters.AuthenticationType != null) expiredParameters.AuthenticationType = ValidationParameters.AuthenticationType;

                return expiredParameters;
            }
            set => expiredValidationParameters = value;
        }
    }
}
