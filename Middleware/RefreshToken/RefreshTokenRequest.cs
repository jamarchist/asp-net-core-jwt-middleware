﻿namespace Middleware.RefreshToken
{
    public class RefreshTokenRequest
    {
        public string BearerToken { get; set; }
        public string RefreshToken { get; set; }
    }
}