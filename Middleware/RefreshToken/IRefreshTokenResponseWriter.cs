﻿using System;
using Microsoft.AspNetCore.Http;

namespace Middleware.RefreshToken
{
    public interface IRefreshTokenResponseWriter
    {
        void WriteResponseForSuccessfulExchange(HttpContext httpContext, RefreshTokenResult exchangeResult);
        void WriteResponseForInvalidRefreshToken(HttpContext httpContext);
        void WriteResponseForError(HttpContext httpContext, Exception error);
    }
}