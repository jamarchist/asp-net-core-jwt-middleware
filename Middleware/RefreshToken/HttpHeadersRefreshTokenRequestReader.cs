﻿using Microsoft.AspNetCore.Http;

namespace Middleware.RefreshToken
{
    public class HttpHeadersRefreshTokenRequestReader : IRefreshTokenRequestReader
    {
        public RefreshTokenRequest Read(HttpContext httpContext)
        {
            var bearerToken = httpContext.Request.Headers["Authorization"].ToString().Substring("Bearer ".Length);
            var refreshToken = httpContext.Request.Headers["RefreshToken"].ToString();

            return new RefreshTokenRequest
            {
                BearerToken = bearerToken,
                RefreshToken = refreshToken
            };
        }
    }
}