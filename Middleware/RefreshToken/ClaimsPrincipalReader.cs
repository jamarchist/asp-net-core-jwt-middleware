﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.Extensions.Options;

namespace Middleware.RefreshToken
{
    public class ClaimsPrincipalReader : IClaimsPrincipalReader
    {
        private readonly TokenValidationOptions options;

        public ClaimsPrincipalReader(IOptions<TokenValidationOptions> options)
        {
            this.options = options.Value;
        }

        public ClaimsPrincipal ReadClaimsFromExpiredToken(string token)
        {
            return new JwtSecurityTokenHandler().ValidateToken(token, options.ExpiredValidationParameters, out var securityToken);
        }

        public ClaimsPrincipal ReadClaimsFromValidToken(string token)
        {
            return new JwtSecurityTokenHandler().ValidateToken(token, options.ValidationParameters, out var securityToken);
        }

        public string ReadUserNameFromExpiredToken(string token)
        {
            return ReadUserNameFromClaimsPrincipal(ReadClaimsFromExpiredToken(token));
        }

        public string ReadUserNameFromValidToken(string token)
        {
            return ReadUserNameFromClaimsPrincipal(ReadClaimsFromValidToken(token));
        }

        private string ReadUserNameFromClaimsPrincipal(ClaimsPrincipal claimsPrincipal)
        {
            return claimsPrincipal.FindFirst(c => c.Type == ClaimTypes.Name).Value;
        }
    }
}