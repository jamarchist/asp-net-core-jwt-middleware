﻿namespace Middleware.RefreshToken
{
    public class RefreshTokenResult
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public bool RefreshTokenIsValid { get; set; }
    }
}