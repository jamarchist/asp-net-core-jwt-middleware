﻿using Microsoft.AspNetCore.Http;

namespace Middleware.RefreshToken
{
    public interface IRefreshTokenRequestReader
    {
        RefreshTokenRequest Read(HttpContext httpContext);
    }
}
