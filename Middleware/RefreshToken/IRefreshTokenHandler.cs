﻿namespace Middleware.RefreshToken
{
    public interface IRefreshTokenHandler
    {
        RefreshTokenResult ExchangeRefreshToken(string user, string refreshToken);
    }
}
