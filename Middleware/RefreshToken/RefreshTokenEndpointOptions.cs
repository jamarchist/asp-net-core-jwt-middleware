﻿namespace Middleware.RefreshToken
{
    public class RefreshTokenEndpointOptions
    {
        public RefreshTokenEndpointOptions()
        {
            Path = "/refresh-token";
        }

        public string Path { get; set; }
    }
}