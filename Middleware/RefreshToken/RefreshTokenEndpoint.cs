﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Middleware.RefreshToken
{
    public class RefreshTokenEndpoint
    {
        private readonly RequestDelegate next;

        public RefreshTokenEndpoint(RequestDelegate next)
        {
            this.next = next;
        }

        public Task Invoke(HttpContext httpContext, IRefreshTokenHandler refreshHandler, IClaimsPrincipalReader tokenReader, IRefreshTokenRequestReader requestReader, IRefreshTokenResponseWriter responseWriter)
        {
            try
            {
                var refreshRequest = requestReader.Read(httpContext);
                var userName = tokenReader.ReadUserNameFromExpiredToken(refreshRequest.BearerToken);

                var exchangeResult = refreshHandler.ExchangeRefreshToken(userName, refreshRequest.RefreshToken);
                if (!exchangeResult.RefreshTokenIsValid)
                {
                    responseWriter.WriteResponseForInvalidRefreshToken(httpContext);
                }
                else
                {
                    responseWriter.WriteResponseForSuccessfulExchange(httpContext, exchangeResult);
                }
            }
            catch (Exception e)
            {
                responseWriter.WriteResponseForError(httpContext, e);
            }

            return Task.CompletedTask;
        }
    }
}