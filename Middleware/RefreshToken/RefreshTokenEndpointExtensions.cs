﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Middleware.RefreshToken
{
    public static class RefreshMiddlewareExtensions
    {
        public static IApplicationBuilder UseRefreshTokenEndpoint(this IApplicationBuilder builder, Action<RefreshTokenEndpointOptions> options = null)
        {
            var configuration = new RefreshTokenEndpointOptions();
            var configureOptions = options ?? (o => { });
            configureOptions(configuration);

            return builder.Map(configuration.Path, app => { app.UseMiddleware<RefreshTokenEndpoint>(); });
        }

        public static IServiceCollection AddRefreshTokenEndpoint<TRefreshTokenHandler>(this IServiceCollection services, Action<TokenValidationOptions> options = null) where TRefreshTokenHandler : class, IRefreshTokenHandler
        {
            services.AddTransient<IClaimsPrincipalReader, ClaimsPrincipalReader>();
            services.AddTransient<IRefreshTokenRequestReader, HttpHeadersRefreshTokenRequestReader>();
            services.AddTransient<IRefreshTokenResponseWriter, HttpHeadersRefreshTokenResponseWriter>();
            services.AddTransient<IRefreshTokenHandler, TRefreshTokenHandler>();
            services.Configure<TokenValidationOptions>(options ?? (o => {}));

            return services;
        }
    }
}