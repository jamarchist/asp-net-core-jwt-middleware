﻿using System.Security.Claims;

namespace Middleware.RefreshToken
{
    public interface IClaimsPrincipalReader
    {
        ClaimsPrincipal ReadClaimsFromExpiredToken(string token);
        ClaimsPrincipal ReadClaimsFromValidToken(string token);
        string ReadUserNameFromExpiredToken(string token);
        string ReadUserNameFromValidToken(string token);
    }
}
