﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;

namespace Middleware.AutomaticRefresh
{
    public static class AutomaticRefreshNegotiationExtensions
    {
        public static IApplicationBuilder UseAutomaticRefreshNegotiation(this IApplicationBuilder builder, Action<AutomaticRefreshOptions> options = null)
        {
            var configuration = new AutomaticRefreshOptions();
            var configureOptions = options ?? (o => { });
            configureOptions(configuration);

            return builder.UseMiddleware<AutomaticRefreshNegotiation>(configuration);
        }

        public static JwtBearerEvents AddMarkerOnAuthenticationFailure(this JwtBearerEvents events)
        {
            events.OnAuthenticationFailed = ctx =>
            {
                if (!(ctx.Exception is Microsoft.IdentityModel.Tokens.SecurityTokenExpiredException)) return Task.CompletedTask;

                ctx.HttpContext.Items.Add("TokenExpired", true);
                return Task.CompletedTask;
            };

            return events;
        }
    }
}