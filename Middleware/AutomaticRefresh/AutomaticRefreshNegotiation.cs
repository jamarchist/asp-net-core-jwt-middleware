﻿using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Middleware.AutomaticRefresh
{
    public class AutomaticRefreshNegotiation
    {
        private readonly RequestDelegate next;
        private readonly AutomaticRefreshOptions options;

        public AutomaticRefreshNegotiation(RequestDelegate next, AutomaticRefreshOptions options)
        {
            this.next = next;
            this.options = options;
        }

        public Task Invoke(HttpContext httpContext)
        {
            if (!httpContext.Items.ContainsKey("TokenExpired"))
            {
                return next(httpContext);
            }

            if (!httpContext.Request.Headers.ContainsKey("RefreshToken"))
            {
                httpContext.Response.StatusCode = (int) HttpStatusCode.Unauthorized;
                return Task.CompletedTask;
            }

            var body = new StreamReader(httpContext.Request.Body).ReadToEnd();

            httpContext.Items.Add(typeof(ReroutingParameters).FullName, new ReroutingParameters
            {
                OriginalPath = httpContext.Request.Path,
                OriginalMethod = httpContext.Request.Method,
                OriginalBodyAsString = body
            });

            httpContext.Request.Path = options.RefreshTokenEndpoint;
            httpContext.Request.Method = "GET";

            var result = next(httpContext);

            var isRerouting = httpContext.Items.ContainsKey(typeof(ReroutingParameters).FullName);
            if (!isRerouting)
            {
                return result;
            }

            var reroutingParameters = httpContext.Items[typeof(ReroutingParameters).FullName] as ReroutingParameters;

            httpContext.Request.Method = reroutingParameters.OriginalMethod;
            httpContext.Request.Path = reroutingParameters.OriginalPath;
            httpContext.Request.Body = new MemoryStream(Encoding.UTF8.GetBytes(reroutingParameters.OriginalBodyAsString));
            httpContext.Request.Headers["Authorization"] = $"Bearer {httpContext.Response.Headers["AccessToken"]}";

            return next(httpContext);
        }

        private class ReroutingParameters
        {
            public PathString OriginalPath { get; set; }
            public string OriginalMethod { get; set; }
            public string OriginalBodyAsString { get; set; }
        }
    }
}
