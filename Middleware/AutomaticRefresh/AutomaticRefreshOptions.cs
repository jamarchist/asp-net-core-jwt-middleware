﻿namespace Middleware.AutomaticRefresh
{
    public class AutomaticRefreshOptions
    {
        public AutomaticRefreshOptions()
        {
            RefreshTokenEndpoint = "/refresh-token";
        }

        public string RefreshTokenEndpoint { get; set; }
    }
}