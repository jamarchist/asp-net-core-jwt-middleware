﻿using RestSharp;
using Serilog;
using Shouldly;
using HttpStatusCode = System.Net.HttpStatusCode;

namespace Tests
{
    class when_navigating_to_site_root
    {
        private readonly IRestResponse response;
        private readonly ILogger log;

        public when_navigating_to_site_root(ILogger log)
        {
            this.log = log;
            var client = new RestSharp.RestClient("http://localhost:5000/");
            response = client.Get(new RestRequest("/"));
        }

        public void http_status_should_be_ok()
        {
            response.StatusCode.ShouldBe(HttpStatusCode.OK);
        }

        public void content_should_be_hello_world()
        {
            response.Content.ShouldBe("Hello World!");
        }
    }
}
