﻿using System.Collections.Generic;
using Nancy;

namespace Tests.Inspection
{
    public class RequestHistory
    {
        public IList<NancyContext> Requests { get; } = new List<NancyContext>();
    }
}