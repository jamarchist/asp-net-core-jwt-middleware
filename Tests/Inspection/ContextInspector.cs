﻿using Nancy.Bootstrapper;
using Serilog;

namespace Tests.Inspection
{
    public class ContextInspector : IApplicationStartup
    {
        private readonly RequestHistory history;

        public ContextInspector(RequestHistory history)
        {
            this.history = history;
        }

        public void Initialize(IPipelines pipelines)
        {
            pipelines.AfterRequest.AddItemToEndOfPipeline(ctx =>
            {
                history.Requests.Add(ctx);
            });
        }
    }
}
