﻿using System.Linq;
using System.Net;
using MediatR;
using RestSharp;
using Shouldly;
using Tests.Actions;
using Tests.Inspection;

namespace Tests
{
    class when_valid_access_token_is_provided_in_request_for_protected_resource
    {
        private readonly RequestHistory history;
        private readonly IRestResponse accessResponse;

        public when_valid_access_token_is_provided_in_request_for_protected_resource(IMediator mediator, RequestHistory history)
        {
            this.history = history;

            var response = mediator.Send(new AttemptLogin{ UserName = "user", Password = "pass"}).Result;
            var token = mediator.Send(new ExtractAccessToken {HttpResponse = response}).Result;
            accessResponse = mediator.Send(new AttemptAccessWithToken { Resource = "/protected", AccessToken = token }).Result;
        }

        public void claims_principal_should_not_be_null()
        {
            history.Requests[0].CurrentUser.ShouldNotBeNull();
        }

        public void http_status_code_should_be_ok()
        {
            accessResponse.StatusCode.ShouldBe(HttpStatusCode.OK);
        }

        public void authorization_should_have_bearer()
        {
            history.Requests[0].Request.Headers.Authorization.ShouldStartWith("Bearer ");
        }

        public void bearer_should_not_be_empty()
        {
            history.Requests[0].Request.Headers.Authorization.Length.ShouldBeGreaterThan("Bearer ".Length);
        }

        public void claims_should_include_username()
        {
            history.Requests[0].CurrentUser.Claims.ShouldContain(c => c.Value == "user");
        }
    }
}
