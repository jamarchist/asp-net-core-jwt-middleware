﻿using System;
using System.Linq;
using System.Net;
using MediatR;
using RestSharp;
using Shouldly;
using Tests.Actions;
using Tests.Inspection;
using Tests.Utility;

namespace Tests
{
    class when_access_token_is_expired_and_refresh_token_is_valid
    {
        private readonly RequestHistory history;
        private readonly IRestResponse accessResponse;

        public when_access_token_is_expired_and_refresh_token_is_valid(IMediator mediator, CalculatedSystemTime dateTime, RequestHistory history)
        {
            this.history = history;

            dateTime.UtcNowCalculator = () => DateTime.UtcNow.AddMinutes(-31);
            var loginResponse = mediator.Send(new AttemptLogin { UserName = "user", Password = "pass" }).Result;
            dateTime.Reset();

            var access_token = mediator.Send(new ExtractAccessToken { HttpResponse = loginResponse }).Result;
            var refresh_token = mediator.Send(new ExtractRefreshToken {HttpResponse = loginResponse}).Result;

            accessResponse = mediator.Send(new AttemptAccessWithToken
            {
                AccessToken = access_token,
                RefreshToken = refresh_token,
                Resource = "/protected"
            }).Result;
        }

        public void there_should_be_one_nancy_request()
        {
            history.Requests.Count.ShouldBe(1);
        }

        public void http_status_should_be_ok()
        {
            accessResponse.StatusCode.ShouldBe(HttpStatusCode.OK);
        }

        public void initial_request_should_be_redirected_to_refresh_endpoint()
        {
            history.Requests.First().Request.Path.ShouldBe("/protected");
        }

        public void final_request_should_be_for_protected_resource()
        {
            history.Requests.Last().Request.Path.ShouldBe("/protected");
        }

        public void response_headers_should_contain_access_token()
        {
            accessResponse.Headers.ShouldContain(h => h.Name == "AccessToken");
        }

        public void response_headers_should_contain_refresh_token()
        {
            accessResponse.Headers.ShouldContain(h => h.Name == "RefreshToken");
        }
    }
}