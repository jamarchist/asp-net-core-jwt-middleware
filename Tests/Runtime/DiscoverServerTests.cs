﻿using Fixie;

namespace Tests.Runtime
{
    public class DiscoverServerTests : Discovery
    {
        public DiscoverServerTests()
        {
            Classes.Where(t => t.Name.StartsWith("when"));

            Methods.Where(t => t.IsPublic && t.IsVoid());
        }
    }
}
