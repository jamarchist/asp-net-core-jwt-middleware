﻿namespace Tests.Runtime
{
    public interface IncludeModule<TModule> where TModule : Autofac.Module, new()
    {
    }
}
