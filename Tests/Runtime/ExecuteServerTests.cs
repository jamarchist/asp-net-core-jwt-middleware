﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using Autofac;
using Autofac.Core;
using Fixie;
using Microsoft.AspNetCore.Hosting;
using Serilog;

namespace Tests.Runtime
{
    class ExecuteServerTests : Execution
    {
        public void Execute(TestClass testClass)
        {
            var scenarioSpecificModules = testClass.Type.GetInterfaces()
                .Where(i => i.IsClosedTypeOf(typeof(IncludeModule<>)))
                .Select(i => i.GenericTypeArguments.First())
                    .ToList();

            void ConfigureContainer(ContainerBuilder builder)
            {
                builder.RegisterType(testClass.Type).AsSelf();
                builder.RegisterAssemblyModules(typeof(ExecuteServerTests).Assembly);
                foreach (var scenarioModule in scenarioSpecificModules)
                {
                    builder.RegisterModule(Activator.CreateInstance(scenarioModule) as IModule);
                }
            }

            var coordinator = new CancellationTokenSource();
            var host = Server.Program.CreateWebHostBuilder(new string[] { }, ConfigureContainer)
                .UseKestrel()
                .UseContentRoot(@"C:\Users\james\Code\NancyAspNetCoreJwtBearerAuth\Server")
                .Build();
            host.RunAsync(coordinator.Token);

            var container = host.Services.GetService(typeof(ILifetimeScope)) as ILifetimeScope;
            
            var scenarioConstruction = TryExecuteScenario(testClass, container);
            TryMakeAssertions(scenarioConstruction, testClass);
            TryDisposeScenario(scenarioConstruction);
            
            host.StopAsync(coordinator.Token);
            coordinator.Cancel(false);
        }

        private ConstructionResult TryExecuteScenario(TestClass testClass, ILifetimeScope container)
        {
            try
            {
                var log = container.Resolve<ILogger>();
                log.LogScenarioStarting(testClass.Type);
                var scenario = container.Resolve(testClass.Type);
                log.LogScenarioComplete(testClass.Type);
                return new ConstructionResult {Success = true, Scenario = scenario};
            }
            catch (Exception constructionOrExecutionException)
            {
                FailEachAssertion(testClass, constructionOrExecutionException);
                return new ConstructionResult {Scenario = null, Success = false};
            }
        }

        private void TryMakeAssertions(ConstructionResult scenarioConstruction, TestClass testClass)
        {
            if (scenarioConstruction.Success)
            {
                testClass.RunCases(@case => @case.Execute(scenarioConstruction.Scenario));
            }
        }

        private void TryDisposeScenario(ConstructionResult scenarioConstruction)
        {
            if (scenarioConstruction.Success)
            {
                if (scenarioConstruction.Scenario is IDisposable disposable)
                {
                    disposable.Dispose();
                }
            }
        }

        private void FailEachAssertion(TestClass testClass, Exception reason)
        {
            testClass.RunCases(@case => @case.Fail(reason));
        }

        private class ConstructionResult { public bool Success { get; set; } public object Scenario { get; set; } }
    }

    internal static class ScenarioLoggingExtensions
    {
        public static void LogScenarioStarting(this ILogger log, Type scenario)
        {
            log.LogScenarioBoxMessage(scenario, "execution starting");
        }

        public static void LogScenarioComplete(this ILogger log, Type scenario)
        {
            log.LogScenarioBoxMessage(scenario, "execution complete");
        }

        private static void LogScenarioBoxMessage(this ILogger log, Type scenario, string message)
        {
            var boxHorizontalEdge = '\u2500';
            var boxVerticalEdge = '\u2502';
            var topLeft = '\u250c';
            var bottomLeft = '\u2514';
            var topRight = '\u2510';
            var bottomRight = '\u2518';

            var centerLine = $"{boxVerticalEdge} {scenario.Name} {message} {boxVerticalEdge}";
            var dashesLength = centerLine.Length - 2;
            var dashes = new string(boxHorizontalEdge, dashesLength);

            log.Information($"{topLeft}{dashes}{topRight}");
            log.Information(centerLine);
            log.Information($"{bottomLeft}{dashes}{bottomRight}");
        }
    }
}
