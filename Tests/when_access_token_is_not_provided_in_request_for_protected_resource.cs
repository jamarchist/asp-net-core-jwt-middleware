﻿using System;
using System.Linq;
using System.Net;
using MediatR;
using RestSharp;
using Shouldly;
using Tests.Actions;
using Tests.Inspection;

namespace Tests
{
    class when_access_token_is_not_provided_in_request_for_protected_resource
    {
        private readonly IRestResponse response;
        private readonly RequestHistory history;

        public when_access_token_is_not_provided_in_request_for_protected_resource(IMediator mediator, RequestHistory history)
        {
            this.history = history;
            response = mediator.Send(new AttemptAccessWithToken { Resource = "/protected", AccessToken = String.Empty }).Result;
        }

        public void http_status_code_should_be_unauthorized()
        {
            response.StatusCode.ShouldBe(HttpStatusCode.Unauthorized);
        }

        public void claims_principal_should_be_null()
        {
            history.Requests[0].CurrentUser.ShouldBeNull();
        }

        public void response_should_contain_www_authenticate_header_with_bearer_challenge()
        {
            response.Headers.ShouldContain(h => h.Name == "WWW-Authenticate");
        }
    }
}
