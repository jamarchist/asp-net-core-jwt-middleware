﻿using System.Net;
using MediatR;
using RestSharp;
using Shouldly;
using Tests.Actions;

namespace Tests
{
    class when_posting_to_protected_resource
    {
        private readonly IRestResponse postResponse;

        public when_posting_to_protected_resource(IMediator mediator)
        {
            var response = mediator.Send(new AttemptLogin { UserName = "user", Password = "pass" }).Result;
            var token = mediator.Send(new ExtractAccessToken { HttpResponse = response }).Result;
            postResponse = mediator.Send(new AttemptPostWithToken
            {
                Resource = "/protected-post",
                AccessToken = token,
                Body = new { message = "Hello protected post" }
            }).Result;
        }

        public void http_status_code_should_be_ok()
        {
            postResponse.StatusCode.ShouldBe(HttpStatusCode.OK);
        }
    }
}