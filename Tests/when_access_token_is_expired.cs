﻿using System;
using System.Linq;
using System.Net;
using MediatR;
using RestSharp;
using Shouldly;
using Tests.Actions;
using Tests.Inspection;
using Tests.Utility;

namespace Tests
{
    class when_access_token_is_expired
    {
        private readonly RequestHistory history;
        private readonly IRestResponse accessResponse;

        public when_access_token_is_expired(IMediator mediator, CalculatedSystemTime dateTime, RequestHistory history)
        {
            this.history = history;
            dateTime.UtcNowCalculator = () => DateTime.UtcNow.AddMinutes(-31);
            var loginResponse = mediator.Send(new AttemptLogin {UserName = "user", Password = "pass"}).Result;
            dateTime.Reset();
            var access_token = mediator.Send(new ExtractAccessToken {HttpResponse = loginResponse}).Result;
            accessResponse = mediator.Send(new AttemptAccessWithToken{AccessToken = access_token, Resource = "/protected"}).Result;
        }

        public void http_status_should_be_unauthorized()
        {
            accessResponse.StatusCode.ShouldBe(HttpStatusCode.Unauthorized);
        }

        public void protected_resource_should_not_be_invoked()
        {
            history.Requests.Count.ShouldBe(0);
            //history.Requests.Last().Request.Path.ShouldBe("/login");
        }
    }
}
