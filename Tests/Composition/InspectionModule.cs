﻿using Autofac;
using Tests.Inspection;

namespace Tests.Composition
{
    class InspectionModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<RequestHistory>().SingleInstance();
            builder.RegisterType<InspectionCache>()
                .SingleInstance();
        }
    }
}
