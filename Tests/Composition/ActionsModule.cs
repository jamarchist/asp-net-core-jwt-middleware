﻿using Autofac;
using MediatR;

namespace Tests.Composition
{
    class ActionsModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(ThisAssembly)
                .Where(t => t.IsClosedTypeOf(typeof(IRequestHandler<>)) ||
                            t.IsClosedTypeOf(typeof(IRequestHandler<,>)) ||
                            t.IsClosedTypeOf(typeof(AsyncRequestHandler<>)) ||
                            t.IsClosedTypeOf(typeof(RequestHandler<>)) ||
                            t.IsClosedTypeOf(typeof(RequestHandler<,>)))
                .AsImplementedInterfaces();
        }
    }
}
