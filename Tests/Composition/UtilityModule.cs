﻿using Autofac;
using Server.Utility;
using Tests.Utility;

namespace Tests.Composition
{
    public class UtilityModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CalculatedSystemTime>()
                .As<ISystemTime>()
                .AsSelf()
                    .SingleInstance();
        }
    }
}
