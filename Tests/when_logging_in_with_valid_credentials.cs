﻿using System.Net;
using MediatR;
using RestSharp;
using Shouldly;
using Tests.Actions;

namespace Tests
{
    class when_logging_in_with_valid_credentials
    {
        private readonly IRestResponse response;
        private readonly IMediator mediator;

        public when_logging_in_with_valid_credentials(IMediator mediator)
        {
            this.mediator = mediator;
            response = mediator.Send(new AttemptLogin
            {
                UserName = "user",
                Password = "pass"
            }).Result;
        }

        public void http_status_code_should_be_ok()
        {
            response.StatusCode.ShouldBe(HttpStatusCode.OK);
        }

        public void access_token_should_be_in_body()
        {
            var access_token = mediator.Send(new ExtractAccessToken {HttpResponse = response}).Result;
            access_token.ShouldNotBeNullOrWhiteSpace();
        }

        public void refresh_token_should_be_in_body()
        {
            var refresh_token = mediator.Send(new ExtractRefreshToken {HttpResponse = response}).Result;
            refresh_token.ShouldNotBeNullOrWhiteSpace();
        }
    }
}
