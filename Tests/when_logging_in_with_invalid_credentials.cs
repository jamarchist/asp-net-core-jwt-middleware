﻿using System.Net;
using MediatR;
using RestSharp;
using Shouldly;
using Tests.Actions;

namespace Tests
{
    class when_logging_in_with_invalid_credentials
    {
        private readonly IRestResponse response;

        public when_logging_in_with_invalid_credentials(IMediator mediator)
        {
            response = mediator.Send(new AttemptLogin
            {
                UserName = "invalid_user",
                Password = "pass"
            }).Result;
        }

        public void http_status_code_should_be_bad_request()
        {
            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
        }

        public void body_should_be_empty()
        {
            response.Content.ShouldBeEmpty();
        }
    }
}
