﻿using System;
using System.Net;
using MediatR;
using RestSharp;
using Shouldly;
using Tests.Actions;
using Tests.Inspection;
using Tests.Utility;

namespace Tests
{
    class when_posting_to_protected_resource_with_refresh_token
    {
        private readonly RequestHistory history;
        private readonly IRestResponse postResponse;

        public when_posting_to_protected_resource_with_refresh_token(IMediator mediator, CalculatedSystemTime dateTime, RequestHistory history)
        {
            this.history = history;
            dateTime.UtcNowCalculator = () => DateTime.UtcNow.AddMinutes(-31);
            var loginResponse = mediator.Send(new AttemptLogin { UserName = "user", Password = "pass" }).Result;
            dateTime.Reset();
            var access_token = mediator.Send(new ExtractAccessToken { HttpResponse = loginResponse }).Result;
            var refresh_token = mediator.Send(new ExtractRefreshToken { HttpResponse = loginResponse }).Result;
            postResponse = mediator.Send(new AttemptPostWithToken
            {
                AccessToken = access_token,
                RefreshToken = refresh_token,
                Resource = "/protected-post",
                Body = new { message = "Hello protected post" }
            }).Result;
        }

        public void http_status_should_be_ok()
        {
            postResponse.StatusCode.ShouldBe(HttpStatusCode.OK);
        }
    }
}