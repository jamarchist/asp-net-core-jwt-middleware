﻿using MediatR;
using RestSharp;

namespace Tests.Actions
{
    public class AttemptLogin : IRequest<IRestResponse>
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class AttemptLoginHandler : RequestHandler<AttemptLogin, IRestResponse>
    {
        protected override IRestResponse Handle(AttemptLogin request)
        {
            var client = new RestClient("http://localhost:5000/");

            var httpRequest = new RestRequest("login", DataFormat.Json);
            httpRequest.AddJsonBody(new
            {
                userName = request.UserName, password = request.Password
            });

            return client.Post(httpRequest);
        }
    }
}
