﻿using MediatR;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace Tests.Actions
{
    public class ExtractJsonField : IRequest<string>
    {
        public IRestResponse HttpResponse { get; set; }
        public string FieldName { get; set; }
    }

    public class ExtractJsonFieldHandler : RequestHandler<ExtractJsonField, string>
    {
        protected override string Handle(ExtractJsonField request)
        {
            var jsonBody = JObject.Parse(request.HttpResponse.Content);
            var fieldValue = jsonBody[request.FieldName].Value<string>();

            return fieldValue;
        }
    }
}