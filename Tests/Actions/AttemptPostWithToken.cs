﻿using System;
using MediatR;
using RestSharp;

namespace Tests.Actions
{
    public class AttemptPostWithToken : IRequest<IRestResponse>
    {
        public string Resource { get; set; }
        public object Body { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }

    public class AttemptPostWithTokenHandler : RequestHandler<AttemptPostWithToken, IRestResponse>
    {
        protected override IRestResponse Handle(AttemptPostWithToken request)
        {
            var client = new RestClient("http://localhost:5000/");
            var httpRequest = new RestRequest(request.Resource, DataFormat.Json);
            httpRequest.AddHeader("Authorization", $"Bearer {request.AccessToken}");
            if (!String.IsNullOrWhiteSpace(request.RefreshToken)) httpRequest.AddHeader("RefreshToken", request.RefreshToken);
            httpRequest.AddJsonBody(request.Body);

            return client.Post(httpRequest);
        }
    }
}