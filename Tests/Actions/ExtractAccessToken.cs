﻿using MediatR;
using RestSharp;

namespace Tests.Actions
{
    public class ExtractAccessToken : IRequest<string>
    {
        public IRestResponse HttpResponse { get; set; }
    }

    public class ExtractAccessTokenHandler : RequestHandler<ExtractAccessToken, string>
    {
        private readonly IMediator mediator;

        public ExtractAccessTokenHandler(IMediator mediator)
        {
            this.mediator = mediator;
        }

        protected override string Handle(ExtractAccessToken request)
        {
            return mediator.Send(new ExtractJsonField
            {
                HttpResponse = request.HttpResponse,
                FieldName = "access_token"
            }).Result;
        }
    }
}