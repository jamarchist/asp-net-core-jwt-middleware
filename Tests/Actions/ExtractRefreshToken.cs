﻿using MediatR;
using RestSharp;

namespace Tests.Actions
{
    public class ExtractRefreshToken : IRequest<string>
    {
        public IRestResponse HttpResponse { get; set; }
    }

    public class ExtractRefreshTokenHandler : RequestHandler<ExtractRefreshToken, string>
    {
        private readonly IMediator mediator;

        public ExtractRefreshTokenHandler(IMediator mediator)
        {
            this.mediator = mediator;
        }
        
        protected override string Handle(ExtractRefreshToken request)
        {
            return mediator.Send(new ExtractJsonField
            {
                HttpResponse = request.HttpResponse,
                FieldName = "refresh_token"
            }).Result;
        }
    }
}