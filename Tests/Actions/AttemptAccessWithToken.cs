﻿using System;
using MediatR;
using RestSharp;

namespace Tests.Actions
{
    public class AttemptAccessWithToken : IRequest<IRestResponse>
    {
        public string Resource { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }

    public class AttemptAccessWithTokenHandler : RequestHandler<AttemptAccessWithToken, IRestResponse>
    {
        protected override IRestResponse Handle(AttemptAccessWithToken request)
        {
            var client = new RestClient("http://localhost:5000/");
            var httpRequest = new RestRequest(request.Resource, DataFormat.Json);
            httpRequest.AddHeader("Authorization", $"Bearer {request.AccessToken}");
            if (!String.IsNullOrWhiteSpace(request.RefreshToken)) httpRequest.AddHeader("RefreshToken", request.RefreshToken);

            return client.Get(httpRequest);
        }
    }
}
