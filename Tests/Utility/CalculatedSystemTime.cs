﻿using System;
using Server.Utility;

namespace Tests.Utility
{
    class CalculatedSystemTime : ISystemTime
    {
        public CalculatedSystemTime()
        {
            SetDefault();
        }

        public Func<DateTime> NowCalculator { get; set; }
        public Func<DateTime> UtcNowCalculator { get; set; }

        public DateTime Now => NowCalculator();
        public DateTime UtcNow => UtcNowCalculator();

        public void Reset()
        {
            SetDefault();
        }

        private void SetDefault()
        {
            NowCalculator = () => DateTime.Now;
            UtcNowCalculator = () => DateTime.UtcNow;
        }
    }
}
