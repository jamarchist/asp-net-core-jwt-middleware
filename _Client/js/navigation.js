import axios from './axios.js'

export default {
    navigateTo(page, controller) {
        const pageUrl = `/pages/${page}.html`
        axios.get(pageUrl).then(partial => {
            const app = document.getElementById('app')
            app.innerHTML = partial.data

            controller.execute()
        })
    }
}