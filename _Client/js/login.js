import axios from './axios.js'
import axiosHelpers from './axios.helpers.js'
import navigation from './navigation.js'
import protectedResource from './protected.js'

export default {
    execute: function () {
        document.getElementById('loginForm').addEventListener('submit', submitEvent => {
            submitEvent.preventDefault()
            const username = document.getElementById('userName').value
            const password = document.getElementById('password').value

            axios.post('/login', {
                userName: username,
                password: password
            }).then(response => {
                axiosHelpers.setTokens(response.data.access_token, response.data.refresh_token)
                navigation.navigateTo('protected', protectedResource)
            }).catch(error => {
                alert(error);
            })
        })
    }
}