import navigation from './navigation.js'
import login from './login.js'

export default {
    start() {
        navigation.navigateTo('login', login)
    }
}