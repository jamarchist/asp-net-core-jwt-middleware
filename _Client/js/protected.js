import axios from './axios.js'

const getProtectedResources = () => {
    const writeResponseToElement = (elementId, response) => {
        document.getElementById(elementId).innerHTML = `${response.status} ${response.statusText}`
    };

    axios.get('/protected').then(response => {
        writeResponseToElement('protected-response', response)
    }).catch(error => {
        writeResponseToElement('protected-response', error.response)
    });

    axios.post('/protected-post', { message: 'blah blah blah' }).then(response => {
        writeResponseToElement('protected-response-post', response)
    }).catch(error => {
        writeResponseToElement('protected-response-post', error.response)
    });

    document.getElementById('timestamp').innerHTML = (new Date()).toString()
}

export default {
    execute: function() {
        getProtectedResources()
        document.getElementById('retry').addEventListener('click', event => {
            getProtectedResources()
        });
    }
}