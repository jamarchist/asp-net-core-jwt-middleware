import app from './app.js'
import axios from './axios.js'
import axiosHelpers from './axios.helpers.js'

axios.interceptors.response.use(response => {
    const accessToken = response.headers.accesstoken
    const refreshToken = response.headers.refreshtoken

    if (response.status === 200 && accessToken) {
        axiosHelpers.setTokens(accessToken, refreshToken)
    }

    return response
});

app.start()