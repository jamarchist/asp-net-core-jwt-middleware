import axios from './axios.js'

export default {
    setTokens: function (accessToken, refreshToken) {
        axios.defaults.headers.common['Authorization'] = `Bearer ${accessToken}`
        axios.defaults.headers.common['RefreshToken'] = `${refreshToken}`

        localStorage.setItem('access_token', accessToken)
        localStorage.setItem('refresh_token', refreshToken)
    }
}