﻿using Nancy;

namespace Server.Endpoints
{
    public class Index : NancyModule
    {
        public Index()
        {
            Get("/", args => View["Client/index.html"]);
        }
    }
}
