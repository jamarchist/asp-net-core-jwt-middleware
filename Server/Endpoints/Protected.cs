﻿using Nancy;
using Nancy.Extensions;
using Nancy.ModelBinding;
using Serilog;
using Server.Authorization;

namespace Server.Endpoints
{
    public class Protected : SecureModule
    {
        public Protected(ILogger log)
        {
            Get("/protected", args =>
            {
                return HttpStatusCode.OK;
            });

            Post("/protected-post", args =>
            {
                log.Information(Request.Body.AsString());

                var posted = this.Bind<PostParameters>();
                if (posted?.Message != null)
                {
                    return HttpStatusCode.OK;
                }

                return HttpStatusCode.BadRequest;
            });
        }

        private class PostParameters
        {
            public string Message { get; set; }
        }
    }
}
