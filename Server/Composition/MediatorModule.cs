﻿using Autofac;
using MediatR;

namespace Server.Composition
{
    public class MediatorModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<Mediator>()
                .As<IMediator>()
                .InstancePerLifetimeScope();

            builder.Register<ServiceFactory>(context =>
            {
                var c = context.Resolve<IComponentContext>();
                return t => c.Resolve(t);
            });

            builder.RegisterAssemblyTypes(typeof(Startup).Assembly)
                .Where(t => t.IsClosedTypeOf(typeof(IRequestHandler<>)) ||
                            t.IsClosedTypeOf(typeof(IRequestHandler<,>)) ||
                            t.IsClosedTypeOf(typeof(AsyncRequestHandler<>)) ||
                            t.IsClosedTypeOf(typeof(RequestHandler<>)) ||
                            t.IsClosedTypeOf(typeof(RequestHandler<,>)))
                .AsImplementedInterfaces();
        }
    }
}
