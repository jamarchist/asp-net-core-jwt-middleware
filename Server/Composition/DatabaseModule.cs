﻿using Autofac;
using Server.Security;

namespace Server.Composition
{
    public class DatabaseModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UserRepository>().SingleInstance();
        }
    }
}
