﻿using Autofac;
using Serilog;

namespace Server.Composition
{
    public class LoggingModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
           builder.RegisterInstance(Log.Logger).As<ILogger>();
        }
    }
}
