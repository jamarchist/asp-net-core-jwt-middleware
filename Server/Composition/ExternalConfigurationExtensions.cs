﻿using Microsoft.Extensions.DependencyInjection;

namespace Server.Composition
{
    public static class ExternalConfigurationExtensions
    {
        public static void AddExternalAutofacConfiguration(this IServiceCollection services,
            IExternalConfiguration externalConfiguration)
        {
            services.AddSingleton<IExternalConfiguration>(externalConfiguration);
        }
    }
}
