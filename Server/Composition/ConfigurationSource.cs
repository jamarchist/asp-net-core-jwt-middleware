﻿using System;
using System.Collections.Generic;
using Autofac;
using Autofac.Builder;
using Autofac.Core;
using Microsoft.Extensions.Configuration;

namespace Server.Composition
{
    public class ConfigurationSource : IRegistrationSource
    {
        public bool IsAdapterForIndividualComponents => false;

        public IEnumerable<IComponentRegistration> RegistrationsFor(Service service, Func<Service, IEnumerable<IComponentRegistration>> registrationAccessor)
        {
            var typedService = service as IServiceWithType;
            if (typedService != null && typedService.ServiceType.IsClass && typedService.ServiceType.Name.EndsWith("Settings"))
            {
                yield return RegistrationBuilder.ForDelegate(
                        (c, p) =>
                        {
                            var instance = Activator.CreateInstance(typedService.ServiceType);
                            var section = typedService.ServiceType.Name.Substring(0, typedService.ServiceType.Name.Length - "Settings".Length);
                            var configuration = c.Resolve<IConfiguration>();

                            configuration.Bind(section, instance);
                            return instance;
                        }
                    ).As(typedService.ServiceType)
                    .CreateRegistration();
            }
        }
    }
}
