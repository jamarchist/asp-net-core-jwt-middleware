﻿using Autofac;

namespace Server.Composition
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterSource(new ConfigurationSource());
        }
    }
}
