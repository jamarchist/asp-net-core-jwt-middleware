﻿using Autofac;

namespace Server.Composition
{
    public interface IExternalConfiguration
    {
        void ConfigureContainer(ContainerBuilder builder);
    }
}
