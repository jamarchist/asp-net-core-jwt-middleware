﻿using Autofac;
using Server.Utility;

namespace Server.Composition
{
    public class UtilityModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SystemTime>().As<ISystemTime>();
        }
    }
}
