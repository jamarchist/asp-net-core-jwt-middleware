﻿using System;
using System.Security.Cryptography;
using MediatR;
using Server.Configuration;
using Server.Utility;

namespace Server.Security
{
    public class GenerateRefreshToken : IRequest<Token> { }

    public class GenerateRefreshTokenHandler : RequestHandler<GenerateRefreshToken, Token>
    {
        private readonly ISystemTime dateTime;
        private readonly AuthenticationSettings config;

        public GenerateRefreshTokenHandler(ISystemTime dateTime, AuthenticationSettings config)
        {
            this.dateTime = dateTime;
            this.config = config;
        }

        protected override Token Handle(GenerateRefreshToken request)
        {
            var buffer = new byte[32];
            var crypto = new RNGCryptoServiceProvider();

            crypto.GetBytes(buffer);
            var value = Convert.ToBase64String(buffer);

            return new Token
            {
                Value = value,
                IsValid = true,
                ExpiresUtc = dateTime.UtcNow.AddMinutes(config.RefreshTokenLifetimeInMinutes)
            };
        }
    }
}
