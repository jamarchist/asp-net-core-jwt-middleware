﻿using System;

namespace Server.Security
{
    public class Token
    {
        public string Value { get; set; }
        public DateTime ExpiresUtc { get; set; }
        public bool IsValid { get; set; }
        public string Replaces { get; set; }
    }
}