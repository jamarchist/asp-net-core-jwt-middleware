﻿using System.Linq;
using MediatR;
using Server.Utility;

namespace Server.Security
{
    public class ReplaceRefreshTokenForUser : IRequest<Token>
    {
        public string UserName { get; set; }
        public string RefreshToken { get; set; }
    }

    public class ReplaceRefreshTokenForUserHandler : RequestHandler<ReplaceRefreshTokenForUser, Token>
    {
        private readonly IMediator mediator;
        private readonly ISystemTime dateTime;

        public ReplaceRefreshTokenForUserHandler(IMediator mediator, ISystemTime dateTime)
        {
            this.mediator = mediator;
            this.dateTime = dateTime;
        }

        protected override Token Handle(ReplaceRefreshTokenForUser request)
        {
            var user = mediator.Send(new FindUserByUserName {UserName = request.UserName}).Result;
            var expiring = user.RefreshTokens.Single(t => t.Value == request.RefreshToken);
            if (expiring.ExpiresUtc > dateTime.UtcNow.AddSeconds(30))
            {
                expiring.ExpiresUtc = dateTime.UtcNow.AddSeconds(30);
            }

            var existing = user.RefreshTokens.SingleOrDefault(t => t.Replaces == request.RefreshToken);
            if (existing != null)
            {
                return existing;
            }

            var freshToken = mediator.Send(new GenerateRefreshToken()).Result;
            freshToken.Replaces = request.RefreshToken;
            user.RefreshTokens.Add(freshToken);

            return freshToken;
        }
    }
}