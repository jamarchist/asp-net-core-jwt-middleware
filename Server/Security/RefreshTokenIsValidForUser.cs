﻿using System.Linq;
using MediatR;
using Server.Utility;

namespace Server.Security
{
    public class RefreshTokenIsValidForUser : IRequest<bool>
    {
        public string UserName { get; set; }
        public string TokenValue { get; set; }
    }

    public class RefreshTokenIsValidForUserHandler : RequestHandler<RefreshTokenIsValidForUser, bool>
    {
        private readonly IMediator mediator;
        private readonly ISystemTime dateTime;

        public RefreshTokenIsValidForUserHandler(IMediator mediator, ISystemTime dateTime)
        {
            this.mediator = mediator;
            this.dateTime = dateTime;
        }

        protected override bool Handle(RefreshTokenIsValidForUser request)
        {
            var user = mediator.Send(new FindUserByUserName { UserName = request.UserName}).Result;
            return user.RefreshTokens.Any(t => t.IsValid && t.Value == request.TokenValue);
        }
    }
}
