﻿using System.Linq;
using MediatR;

namespace Server.Security
{
    public class FindUserByCredentials : IRequest<User>
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class FindUserByCredentialsHandler : RequestHandler<FindUserByCredentials, User>
    {
        private readonly UserRepository users;

        public FindUserByCredentialsHandler(UserRepository users)
        {
            this.users = users;
        }

        protected override User Handle(FindUserByCredentials request)
        {
            return users.Query(u => u.UserName == request.UserName && u.Password == request.Password).FirstOrDefault();
        }
    }
}
