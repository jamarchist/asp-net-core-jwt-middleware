﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using MediatR;
using Microsoft.IdentityModel.Tokens;
using Server.Configuration;
using Server.Utility;

namespace Server.Security
{
    public class GenerateAccessToken : IRequest<string>
    {
        public string UserName { get; set; }
        public IDictionary<string, object> SecurityClaims { get; set; }
    }

    public class GenerateAccessTokenHandler : RequestHandler<GenerateAccessToken, string>
    {
        private readonly AuthenticationSettings config;
        private readonly ISystemTime dateTime;

        public GenerateAccessTokenHandler(AuthenticationSettings config, ISystemTime dateTime)
        {
            this.config = config;
            this.dateTime = dateTime;
        }

        protected override string Handle(GenerateAccessToken request)
        {
            request.SecurityClaims.Add(ClaimTypes.Name, request.UserName);
            var claims = request.SecurityClaims.Select(c => new Claim(c.Key, c.Value.ToString(), ClaimTypeFor(c.Value))).ToArray();

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config.Secret));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer: config.Issuer,
                audience: config.Audience,
                claims: claims,
                expires: dateTime.UtcNow.AddMinutes(config.AccessTokenLifetimeInMinutes),
                signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private string ClaimTypeFor(object value)
        {
            switch (value)
            {
                case bool b:
                    return ClaimValueTypes.Boolean;
                case int i:
                    return ClaimValueTypes.Integer;
                case DateTime d:
                    return ClaimValueTypes.DateTime;
                default:
                    return ClaimValueTypes.String;
            }
        }
    }
}
