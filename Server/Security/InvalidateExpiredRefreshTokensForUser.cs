﻿using MediatR;
using Server.Utility;

namespace Server.Security
{
    public class InvalidateExpiredRefreshTokensForUser : IRequest
    {
        public string UserName { get; set; }
    }

    public class InvalidateExpiredRefreshTokensForUserHandler : RequestHandler<InvalidateExpiredRefreshTokensForUser>
    {
        private readonly IMediator mediator;
        private readonly ISystemTime dateTime;

        public InvalidateExpiredRefreshTokensForUserHandler(IMediator mediator, ISystemTime dateTime)
        {
            this.mediator = mediator;
            this.dateTime = dateTime;
        }

        protected override void Handle(InvalidateExpiredRefreshTokensForUser request)
        {
            var user = mediator.Send(new FindUserByUserName {UserName = request.UserName}).Result;
            foreach (var token in user.RefreshTokens)
            {
                if (token.ExpiresUtc < dateTime.UtcNow)
                {
                    token.IsValid = false;
                }
            }
        }
    }
}