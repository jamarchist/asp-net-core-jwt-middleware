﻿using MediatR;

namespace Server.Security
{
    public class AddRefreshTokenForUser : IRequest
    {
        public string UserName { get; set; }
        public Token RefreshToken { get; set; }
    }

    public class AddRefreshTokenForUserHandler : RequestHandler<AddRefreshTokenForUser>
    {
        private readonly IMediator mediator;

        public AddRefreshTokenForUserHandler(IMediator mediator)
        {
            this.mediator = mediator;
        }

        protected override void Handle(AddRefreshTokenForUser request)
        {
            var user = mediator.Send(new FindUserByUserName {UserName = request.UserName}).Result;
            user.RefreshTokens.Add(request.RefreshToken);
        }
    }
}