﻿using System.Linq;
using MediatR;

namespace Server.Security
{
    public class FindUserByUserName : IRequest<User>
    {
        public string UserName { get; set; }
    }

    public class FindUserByUserNameHandler : RequestHandler<FindUserByUserName, User>
    {
        private readonly UserRepository users;

        public FindUserByUserNameHandler(UserRepository users)
        {
            this.users = users;
        }

        protected override User Handle(FindUserByUserName request)
        {
            return users.Query(u => u.UserName == request.UserName).FirstOrDefault();
        }
    }
}