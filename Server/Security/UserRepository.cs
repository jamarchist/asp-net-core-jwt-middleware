﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Server.Security
{
    public class UserRepository
    {
        private readonly IList<User> users = new List<User>();

        public UserRepository()
        {
            users.Add(new User
            {
                UserName = "user",
                Password = "pass",
                RefreshTokens = new List<Token>()
            });
        }

        public IList<User> Query(Func<User, bool> filter)
        {
            return users.Where(filter).ToList();
        }
    }
}