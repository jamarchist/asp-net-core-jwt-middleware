﻿using System.Collections.Generic;

namespace Server.Security
{
    public class User
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public IList<Token> RefreshTokens { get; set; }
    }
}
