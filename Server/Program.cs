﻿using System;
using Autofac;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Serilog;
using Server.Composition;

namespace Server
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args, Action<ContainerBuilder> configureContainer = null)
        {
            if (configureContainer == null) configureContainer = b => { };
            var mergedConfiguration = new MergedConfiguration(configureContainer);

            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                .CreateLogger();

            return WebHost.CreateDefaultBuilder(args)
                .ConfigureServices(services => services.AddExternalAutofacConfiguration(mergedConfiguration))
                .UseSerilog()
                .UseStartup<Startup>();
        }

        private class MergedConfiguration : IExternalConfiguration
        {
            private readonly Action<ContainerBuilder> configureContainer;

            public MergedConfiguration(Action<ContainerBuilder> configureContainer)
            {
                this.configureContainer = configureContainer;
            }

            public void ConfigureContainer(ContainerBuilder builder)
            {
                builder.RegisterAssemblyModules(typeof(Program).Assembly);
                configureContainer(builder);
            }
        }
    }
}
