﻿using Nancy;
using Nancy.Security;

namespace Server.Authorization
{
    public static class AuthorizationExtensions
    {
        public static void RequiresBearerAuthentication(this NancyModule module)
        {
            module.RequiresAuthentication();
            module.ChallengesForBearerAuthentication();
        }

        public static void ChallengesForBearerAuthentication(this NancyModule module)
        {
            module.After.AddItemToStartOfPipeline(ctx => ctx.Response.Headers.Add("WWW-Authenticate", "Bearer"));
        }
    }
}
