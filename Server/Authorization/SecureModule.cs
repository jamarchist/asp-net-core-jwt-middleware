﻿using Nancy;
using Serilog;

namespace Server.Authorization
{
    public abstract class SecureModule : NancyModule
    {
        protected SecureModule()
        {
            this.RequiresBearerAuthentication();
        }
    }
}
