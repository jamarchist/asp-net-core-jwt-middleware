﻿using System.Collections.Generic;
using MediatR;
using Middleware.Login;
using Server.Security;

namespace Server.Authentication
{
    public class LocalLoginHandler : ILoginHandler
    {
        private readonly IMediator mediator;

        public LocalLoginHandler(IMediator mediator)
        {
            this.mediator = mediator;
        }

        public LoginResult LoginWithPassword(string user, string password)
        {
            var databaseUser = mediator.Send(new FindUserByCredentials
            {
                UserName = user,
                Password = password
            }).Result;

            if (databaseUser == null) return new LoginResult {LoginSucceeded = false};

            var accessToken = mediator.Send(new GenerateAccessToken
            {
                UserName = databaseUser.UserName,
                SecurityClaims = new Dictionary<string, object>()
            }).Result;

            var refreshToken = mediator.Send(new GenerateRefreshToken()).Result;
            mediator.Send(new AddRefreshTokenForUser
            {
                RefreshToken = refreshToken,
                UserName = databaseUser.UserName
            });

            return new LoginResult
            {
                AccessToken = accessToken,
                RefreshToken = refreshToken.Value,
                LoginSucceeded = true
            };
        }
    }
}