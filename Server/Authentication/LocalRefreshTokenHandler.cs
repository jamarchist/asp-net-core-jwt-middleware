﻿using System.Collections.Generic;
using MediatR;
using Middleware.RefreshToken;
using Server.Security;

namespace Server.Authentication
{
    public class LocalRefreshTokenHandler : IRefreshTokenHandler
    {
        private readonly IMediator mediator;

        public LocalRefreshTokenHandler(IMediator mediator)
        {
            this.mediator = mediator;
        }

        public RefreshTokenResult ExchangeRefreshToken(string user, string refreshToken)
        {
            mediator.Send(new InvalidateExpiredRefreshTokensForUser { UserName = user }).Wait();
            var isValid = mediator.Send(new RefreshTokenIsValidForUser { UserName = user, TokenValue = refreshToken }).Result;

            if (!isValid)
            {
                return new RefreshTokenResult
                {
                    RefreshTokenIsValid = false
                };
            }

            var accessToken = mediator.Send(new GenerateAccessToken
            {
                UserName = user,
                SecurityClaims = new Dictionary<string, object>()
            }).Result;
            
            var freshToken = mediator.Send(new ReplaceRefreshTokenForUser
            {
                UserName = user,
                RefreshToken = refreshToken
            }).Result;

            return new RefreshTokenResult
            {
                AccessToken = accessToken,
                RefreshToken = freshToken.Value,
                RefreshTokenIsValid = true
            };
        }
    }
}