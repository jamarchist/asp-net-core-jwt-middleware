﻿using System;

namespace Server.Utility
{
    public class SystemTime : ISystemTime
    {
        public DateTime Now => DateTime.Now;
        public DateTime UtcNow => DateTime.UtcNow;
    }
}
