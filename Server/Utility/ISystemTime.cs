﻿using System;

namespace Server.Utility
{
    public interface ISystemTime
    {
        DateTime Now { get; }
        DateTime UtcNow { get; }
    }
}