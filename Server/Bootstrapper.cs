﻿using Autofac;
using Nancy;
using Nancy.Bootstrappers.Autofac;
using Nancy.Configuration;

namespace Server
{
    public class Bootstrapper : AutofacNancyBootstrapper
    {
        private readonly ILifetimeScope applicationContainer;

        public Bootstrapper(ILifetimeScope applicationContainer)
        {
            this.applicationContainer = applicationContainer;
        }

        protected override ILifetimeScope GetApplicationContainer()
        {
            return this.applicationContainer;
        }

        public override void Configure(INancyEnvironment environment)
        {
            environment.Tracing(true, true);
            base.Configure(environment);
        }
    }
}
