﻿using Microsoft.Extensions.Configuration;

namespace Server.Configuration
{
    public static class ConfigurationExtensions
    {
        public static TSettings Map<TSettings>(this IConfiguration configuration) where TSettings : new()
        {
            var settingsType = typeof(TSettings).Name;
            var section = settingsType.Substring(0, settingsType.Length - "Settings".Length);
            var instance = new TSettings();

            configuration.Bind(section, instance);
            return instance;
        }
    }
}
