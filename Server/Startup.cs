﻿using System;
using System.IO;
using System.Text;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.IdentityModel.Tokens;
using Middleware.AutomaticRefresh;
using Middleware.Login;
using Middleware.RefreshToken;
using Nancy.Owin;
using Server.Authentication;
using Server.Composition;
using Server.Configuration;

namespace Server
{
    public class Startup : IStartup
    {
        private readonly IExternalConfiguration externalConfiguration;
        private readonly IConfiguration configuration;

        public Startup(IExternalConfiguration externalConfiguration, IConfiguration configuration)
        {
            this.externalConfiguration = externalConfiguration;
            this.configuration = configuration;
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            ConfigureAuthentication(services);    

            var containerBuilder = new ContainerBuilder();
            containerBuilder.Populate(services);
            externalConfiguration.ConfigureContainer(containerBuilder);

            var container = containerBuilder.Build();
            return new AutofacServiceProvider(container);
        }

        private void ConfigureAuthentication(IServiceCollection services)
        {
            var authSettings = configuration.Map<AuthenticationSettings>();
            var tokenValidation = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                ValidIssuer = authSettings.Issuer,
                ValidAudience = authSettings.Audience,
                IssuerSigningKey = new SymmetricSecurityKey(
                    Encoding.UTF8.GetBytes(authSettings.Secret)),
                ClockSkew = TimeSpan.Zero
            };

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = tokenValidation;
                    options.Events = new JwtBearerEvents().AddMarkerOnAuthenticationFailure();
                });

            services.AddLoginEndpoint<LocalLoginHandler>();
            services.AddRefreshTokenEndpoint<LocalRefreshTokenHandler>(options => options.ValidationParameters = tokenValidation);
        }

        public void Configure(IApplicationBuilder app)
        {
            var container = app.ApplicationServices.GetRequiredService<ILifetimeScope>();
            var env = app.ApplicationServices.GetRequiredService<IHostingEnvironment>();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Client", "js")),
                RequestPath = "/js"
            });

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Client", "pages")),
                RequestPath = "/pages"
            });

            app.UseLoginEndpoint();
            app.UseAuthentication();
            app.UseAutomaticRefreshNegotiation();
            app.UseRefreshTokenEndpoint();
            app.UseOwin(x => x.UseNancy(o => o.Bootstrapper = new Bootstrapper(container)));
        }
    }
}
